tensorflow-gpu~=2.4.1
Pillow~=8.1.2
matplotlib~=3.3.4
ipython~=7.21.0
numpy~=1.19.5