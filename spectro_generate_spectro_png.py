import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import os
import os.path
from PIL import Image


facesdir = "faces/"
wavdir = "yt_downloads/wavs/"
specdir = "spectrograms/"
concdir = "../pix2pix_spectro/Pix2Pix-tensorflow/sidebyside/train/"


files = os.listdir(wavdir)
# print(files)

plot_size = 512


def genspectrogram(png):
    filename = png.split('.png')
    filename = filename[0]
    filenameext = filename + '.wav'
    # print(filename)
    filepath = wavdir + filenameext
    # print(filepath)
    testspecfilepath = specdir + png

    if(os.path.exists(filepath)):
        if(os.path.exists(testspecfilepath)):
            print(png, ' → a spectrogram of this file already exists')
        else:
            try:
                # print("it exists")
                y, sr = librosa.load(filepath)
                # n_mels = 128
                # n_fft = 2048
                hop_length = 512
                y, _ = librosa.effects.trim(y)
                # S = librosa.feature.melspectrogram(y, sr=sr, n_fft=n_fft, hop_length=hop_length, n_mels=n_mels)
                S = np.abs(librosa.stft(y, hop_length=512))
                S_DB = librosa.power_to_db(S, ref=np.max)
                librosa.display.specshow(S_DB, sr=sr, hop_length=hop_length, x_axis='time', y_axis='mel');
                # plt.colorbar(format='%+2.0f dB');

                # Remove white spaces and labels
                plt.subplots_adjust(top=1, bottom=0, right=1, left=0,
                                    hspace=0, wspace=0)
                plt.margins(0, 0)
                plt.gca().xaxis.set_major_locator(plt.NullLocator())
                plt.gca().yaxis.set_major_locator(plt.NullLocator())
                plt.axis('off')
                # plt.show()
                # filename = file.split('.wav')
                fileext = ".png"
                specfilename = specdir + filename + fileext
                plt.savefig(specfilename)
            except:
                return_msg = 'ERROR, cannot extract spectrogram, moving on'
                return return_msg



def concatenateimgs(png):
    filename = png.split('.png')
    filename = filename[0]
    jpgfile = filename + '.jpg'
    specfilepath = specdir + png
    facefilepath = facesdir + png

    try:
        images = [Image.open(x).resize((plot_size, plot_size)) for x in [facefilepath, specfilepath]]
        widths, heights = zip(*(i.size for i in images))

        total_width = sum(widths)
        max_height = max(heights)

        new_im = Image.new('RGB', (total_width, max_height))

        x_offset = 0
        for im in images:
            new_im.paste(im, (x_offset, 0))
            x_offset += im.size[0]

        new_im.save(concdir + jpgfile)
    except:
        return_msg = 'cannot concatenate'
        return return_msg



def enumthroughfiles(files):
    spec_count = 0
    for png in files:
        spec_count += 1
        print('spectrogram ', spec_count, '/', len(files))
        genspectrogram(png)

    conc_count = 0
    for img in files:
        conc_count += 1
        print('concatenated image ', conc_count, '/', len(files))
        concatenateimgs(img)


if __name__ == '__main__':
    files = os.listdir(facesdir)
    enumthroughfiles(files)
