import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import io
import os
import time
# import PySimpleGUI as sg
from PIL import Image
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-w", "--wav", help="add wav filepath", nargs='?', const='./test_wavs/00001.wav', required=True)
parser.add_argument("-n", "--name", help="output filename", nargs='?', const='wav2face', required=True)
parser.add_argument("-o", "--output", help="output folder", nargs='?', const='./faces_out', required=True)
parser.add_argument("-c", "--ckpt", help="checkpoint directory", nargs='?', const='F:/pix2pix_spec/all_training_checkpoints', required=True)
parser.add_argument("-m", "--model", help="select model", nargs='?', const=73, type=int)
args = parser.parse_args()


tf.compat.v1.enable_eager_execution

testfile = "test_wavs/Voicemail.wav"
model_numb = 60
specdir = "spectrograms_out/"
facedir = "faces_out/"
# ckptdir = "F:/pix2pix_spec/all_training_checkpoints"
ckptdir = args.ckpt
tempdir = "temp/"

max_freq = 11000

BATCH_SIZE = 1
IMG_WIDTH = 512
IMG_HEIGHT = 512
OUTPUT_CHANNELS = 3


def generate_spect(filepath, outdir):
    hop_length = 512
    filename = "spectrogram"
    fileext = ".png"
    plt.figure(figsize=(10,4))
    y, sr = librosa.load(filepath)
    y, _ = librosa.effects.trim(y)
    S = np.abs(librosa.stft(y, hop_length=hop_length))
    S_DB = librosa.power_to_db(S, ref=np.max)
    librosa.display.specshow(S_DB, sr=sr, hop_length=hop_length, fmax=8000, x_axis='time', y_axis='mel')
    freqs = librosa.fft_frequencies()
    max_bin = np.flatnonzero(freqs >= max_freq)[0]
    plt.ylim([0, max_bin])

    # Remove white spaces and labels
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)
    plt.axis('off')
    specfile = outdir + filename + fileext
    plt.savefig(specfile)
    print("spectrogram generated")
    # plt.show()


def load(image_file):
    image = tf.io.read_file(image_file)
    image = tf.image.decode_jpeg(image)

    w = tf.shape(image)[1]

    w = w // 2
    real_image = image[:, :w, :]
    input_image = image[:, w:, :]

    input_image = tf.cast(input_image, tf.float32)
    real_image = tf.cast(real_image, tf.float32)

    return input_image, real_image


def resize(input_image, real_image, height, width):
    input_image = tf.image.resize(input_image, [height, width],
                                  method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    real_image = tf.image.resize(real_image, [height, width],
                                 method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)

    return input_image, real_image


def normalize(input_image, real_image):
    input_image = (input_image / 127.5) - 1
    real_image = (real_image / 127.5) - 1

    return input_image, real_image


def load_image_test(image_file):
    input_image, real_image = load(image_file)
    input_image, real_image = resize(input_image, real_image,
                                     IMG_HEIGHT, IMG_WIDTH)
    input_image, real_image = normalize(input_image, real_image)

    return input_image, real_image


def downsample(filters, size, apply_batchnorm=True):
    initializer = tf.random_normal_initializer(0., 0.02)
    result = tf.keras.Sequential()
    result.add(tf.keras.layers.Conv2D(filters, size, strides=2, padding='same', kernel_initializer=initializer, use_bias=False))

    if apply_batchnorm:
        result.add(tf.keras.layers.BatchNormalization())

    result.add(tf.keras.layers.LeakyReLU())

    return result


def upsample(filters, size, apply_dropout=False):
    initializer = tf.random_normal_initializer(0., 0.02)

    result = tf.keras.Sequential()
    result.add(
        tf.keras.layers.Conv2DTranspose(filters, size, strides=2,
                                        padding='same',
                                        kernel_initializer=initializer,
                                        use_bias=False))

    result.add(tf.keras.layers.BatchNormalization())

    if apply_dropout:
        result.add(tf.keras.layers.Dropout(0.5))

    result.add(tf.keras.layers.ReLU())

    return result


def Generator():
    inputs = tf.keras.layers.Input(shape=[512, 512, 3])

    down_stack = [
        downsample(64, 4, apply_batchnorm=False),  # (bs, 128, 128, 64)
        downsample(128, 4),  # (bs, 64, 64, 128)
        downsample(256, 4),  # (bs, 32, 32, 256)
        downsample(512, 4),  # (bs, 16, 16, 512)
        downsample(512, 4),  # (bs, 8, 8, 512)
        downsample(512, 4),  # (bs, 4, 4, 512)
        downsample(512, 4),  # (bs, 2, 2, 512)
        downsample(512, 4),  # (bs, 1, 1, 512)
    ]

    up_stack = [
        upsample(512, 4, apply_dropout=True),  # (bs, 2, 2, 1024)
        upsample(512, 4, apply_dropout=True),  # (bs, 4, 4, 1024)
        upsample(512, 4, apply_dropout=True),  # (bs, 8, 8, 1024)
        upsample(512, 4),  # (bs, 16, 16, 1024)
        upsample(256, 4),  # (bs, 32, 32, 512)
        upsample(128, 4),  # (bs, 64, 64, 256)
        upsample(64, 4),  # (bs, 128, 128, 128)
    ]

    initializer = tf.random_normal_initializer(0., 0.02)
    last = tf.keras.layers.Conv2DTranspose(OUTPUT_CHANNELS, 4,
                                           strides=2,
                                           padding='same',
                                           kernel_initializer=initializer,
                                           activation='tanh')  # (bs, 256, 256, 3)

    x = inputs

    # Downsampling through the model
    skips = []
    for down in down_stack:
        x = down(x)
        skips.append(x)

    skips = reversed(skips[:-1])

    # Upsampling and establishing the skip connections
    for up, skip in zip(up_stack, skips):
        x = up(x)
        x = tf.keras.layers.Concatenate()([x, skip])

    x = last(x)

    return tf.keras.Model(inputs=inputs, outputs=x)


def Discriminator():
    initializer = tf.random_normal_initializer(0., 0.02)

    inp = tf.keras.layers.Input(shape=[512, 512, 3], name='input_image')
    tar = tf.keras.layers.Input(shape=[512, 512, 3], name='target_image')

    x = tf.keras.layers.concatenate([inp, tar])  # (bs, 256, 256, channels*2)

    down1 = downsample(64, 4, False)(x)  # (bs, 128, 128, 64)
    down2 = downsample(128, 4)(down1)  # (bs, 64, 64, 128)
    down3 = downsample(256, 4)(down2)  # (bs, 32, 32, 256)
    down3 = downsample(512, 4)(down2)  # (bs, 32, 32, 256)

    zero_pad1 = tf.keras.layers.ZeroPadding2D()(down3)  # (bs, 34, 34, 256)
    conv = tf.keras.layers.Conv2D(512, 4, strides=1,
                                  kernel_initializer=initializer,
                                  use_bias=False)(zero_pad1)  # (bs, 31, 31, 512)

    batchnorm1 = tf.keras.layers.BatchNormalization()(conv)

    leaky_relu = tf.keras.layers.LeakyReLU()(batchnorm1)

    zero_pad2 = tf.keras.layers.ZeroPadding2D()(leaky_relu)  # (bs, 33, 33, 512)

    last = tf.keras.layers.Conv2D(1, 4, strides=1,
                                  kernel_initializer=initializer)(zero_pad2)  # (bs, 30, 30, 1)

    return tf.keras.Model(inputs=[inp, tar], outputs=last)


def generate_result_image(model, test_input, tar, Num, dir, name):
    prediction = model(test_input, training=False)
    fig = plt.figure(figsize=(15, 15), frameon=False)
    plt.imshow(prediction[0] * 0.5 + 0.5)
    plt.axis('off')
    # plt.show()
    plt.savefig(dir + name + "-" + str(Num) + '.png', bbox_inches='tight', pad_inches=0)
    # plt.savefig(facedir + 'face_' +str(Num) +'.png', bbox_inches='tight', pad_inches=0)
    # plt.savefig('./Test/Drw_' + str(Num) + '.png', bbox_inches='tight', pad_inches=0)
    # fig.clear()
    plt.close(fig)


def generate(modelID, generator, dir, name):
    spectropath = specdir + "spectrogram.png"
    newImg = Image.new('RGB', (1024, 1024))
    with Image.open(spectropath) as im:
        # im.convert('RGB')
        img = im.resize((1024, 1024))
        newImg.paste(img)
        newImg.save(tempdir + "spectrogram.png")

    datasetfile = tf.data.Dataset.list_files(tempdir + 'spectrogram*')
    datasetfile = datasetfile.map(load_image_test)
    datasetfile = datasetfile.batch(BATCH_SIZE)

    ID = modelID
    for inp, tar in datasetfile.take(1):
        generate_result_image(generator, inp, tar, ID, dir, name)


def loadModel(modelID, dir, name):
    generator = Generator()
    discriminator = Discriminator()
    generator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
    discriminator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)

    checkpoint = tf.train.Checkpoint(generator_optimizer=generator_optimizer,
                                     discriminator_optimizer=discriminator_optimizer,
                                     generator=generator,
                                     discriminator=discriminator)

    ckpt_path = ckptdir + "ckpt-" + str(args.model)
    print(ckpt_path)
    # checkpoint.restore(tf.train.latest_checkpoint(ckptdir))
    checkpoint.restore(ckpt_path)
    generate(modelID, generator, dir, name)


if __name__ == "__main__":
    generate_spect(args.wav, specdir)
    loadModel(args.model, args.output, args.name)